<?php
$config['pages'] =[
    'index' => [
        'layout' => 'index',
        'blocks' => [
            'content' => [
                'modules-list' => [
                    'class'  => \Modules\Html::class,
                    'template' => 'html/static/index.twig'
                ]
            ]
        ],
    ],
    'queue' => [
        'layout' => 'three_columns',
        'title'  => 'content api',
        'blocks' => [
            'column-2' => [
                'queue' => [
                    'class'  => \Modules\Sys\Queue::class,
                    'action' => 'status',
                    'template' => 'sys/queue/status.twig'
                ]
            ]
        ]
    ],
    'content_api_test' => [
        'layout' => 'three_columns',
        'title'  => 'content api',
        'blocks' => [
            'column-2' => [
                'test' => [
                    'class'  => \Modules\Torg\Api\Content::class,
                    'action' => 'test',
                    'template' => 'content_api/test.twig'
                ]
            ]
        ]
    ],
    'post_item' => [
        'layout' => 'three_columns',
        'title'  => 'hello',
        'blocks' => [
            'column-2' => [
                'post' => [
                    'class'  => \Modules\Posts\Post::class,
                    'action' => 'showItem',
                    'template' => 'post/showItem.twig'
                ]
            ]
        ]
    ]
];

$config['defaults'] = [
    'index' => [
        'css' => [
            'main.css',
            'style.css',
        ],
        'js' => [
            'main.js'
        ]
    ]
];

return $config;
