<?php

return [
    'request'          => [
        'class' => \Application\Http\Request::class,
    ],
    'httpClient'       => [
        'class' => \Application\Http\Client::class,
    ],
    'db'               => [
        'class' => \Application\Database\Factory::class,
    ],
    'router'           => [
        'class' => \Application\Routing\Router::class,
    ],
    'controller'       => [
        'class' => \Application\Controller\Web::class,
    ],
    'view'             => [
        'class' => \Application\View\Helper::class,
    ],
    'injector'         => [
        'class' => \Application\Injector::class
    ],
    'contentapiClient' => [
        'class' => Application\Torg\Component\ContentApi\Client::class,
    ],
    'bll'              => [
        'class'            => BLL\Factory::class,
        'componentsFolder' => 'bll',
        'posts'            => [
            'class' => BLL\Posts\Post::class,
        ],
        'queueAuthor'      => [
            'class' => BLL\Queue\Author::class,
        ],
        'author'           => [
            'class' => BLL\Authors\Author::class,
        ],
        'worker'           => [
            'class' => BLL\Queue\Worker::class,
        ],
    ]
];