<?php
return [
    '' => [
        ''        => 'index',
        'post'    => [
            '%s' => [
                '_var' => 'username',
                '%d'   => [
                    '_var' => 'postId',
                    ''     => 'post_item'
                ]
            ]
        ],
        'sys'     => [
            'queue' => 'queue',
        ],
        'content' => [
            '%s' => [
                '_var' => 'method',
                ''     => 'content_api_test'
            ]
        ]
    ],
];