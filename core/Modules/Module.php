<?php
namespace Modules;

abstract class Module
{
    /**
     * @var array
     */
    private $variables = [];

    public function __construct(array $variables)
    {
        $this->variables = $variables;
    }

    protected function getUriVariable($variableName, $default = null)
    {
        return $this->variables[$variableName] ?? $default;
    }
}