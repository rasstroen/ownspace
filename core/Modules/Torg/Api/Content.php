<?php
namespace Modules\Torg\Api;

use Application\Configuration;
use Application\Http\Request;
use Application\Torg\Component\ContentApi\Client;
use Modules\Module;

class Content extends Module
{
    /**
     * @var Client
     */
    private $apiClient = 'contentapiClient';

    /**
     * @var Configuration
     */
    private $configuration = 'configuration';

    /**
     * @var Request
     */
    private $request = 'request';

    public function actionTest()
    {
        $request = '/1.0/category/1/children.json?geo_id=25';
        $request = $this->request->getPostParam('request', $request);
        $this->apiClient->setBaseUrl($this->configuration->getSetting('torg_api_url'));
        $this->apiClient->setToken($this->configuration->getSetting('torg_api_secret'));
        $response = '';
        $json = '';
        try {
            $json = $this->apiClient->request($request, []);
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }
        if ($json) {
            $response = json_decode($json, true);
        }
        return [
            'request'  => $request,
            'url'      => $this->apiClient->getQueryString(),
            'json'     => print_r($json, true),
            'response' => print_r($response, true),
            'error'    => $error
        ];
    }
}