<?php
namespace Modules\Sys;

use Application\Command\Queue\Server;
use Modules\Module;

class Queue extends Module
{
    /**
     * @var \Application\Configuration
     */
    private $configuration = 'configuration';

    public function actionStatus()
    {
        list($time, $pid, $file) = file($this->configuration->getSetting('queue')['status_file']);
        $data = json_decode($file, true);
        if (!empty($data['workersStatus'])) {
            foreach ($data['workersStatus'] as &$worker) {
                if (!empty($worker['workers'])) {
                    $worker['workers_count'] = count($worker['workers']);
                } else {
                    $worker['workers_count'] = 0;
                }
            }
            unset($worker);
        } else {
            $data = [];
        }
        return [
            'date'   => date('Y-m-d H:i:s', (int)$time),
            'pid'    => $pid,
            'status' => $data,
        ];
    }
}