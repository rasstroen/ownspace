<?php
namespace Modules\Posts;

use Modules\Module;

class Post extends Module
{
    /**
     * @var \BLL\Posts\Post
     */
    private $bllPosts = 'bll.posts';

    public function actionShowItem()
    {
        $post = $this->bllPosts->getByPostIdUsername(
            $this->getUriVariable('postId'),
            $this->getUriVariable('username')
        );


        return [
            'title'  => $post['title'] . '[' . $this->getUriVariable('postId') . ']',
            'author' => [
                'nickname' => $this->getUriVariable('username', 'unknown author'),
            ],
            'text'   => 'some text'
        ];
    }
}