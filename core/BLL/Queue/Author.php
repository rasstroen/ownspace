<?php

namespace BLL\Queue;

use Application\Command\Queue\Server;
use Application\Database\Factory;
use BLL\Base;

class Author extends Base
{
    /**
     * @var Factory
     */
    private $db = 'db';

    /**
     * @var Worker
     */
    private $bllWorker = 'bll.worker';

    public function addTaskToProcessAuthorFeed(int $authorId, int $delay = 0)
    {
        $taskId = $authorId;
        $this->bllWorker->addTask(Server::WORKER_ID_AUTHOR_FETCH_RSS, $taskId, [$authorId], $delay);
    }
}