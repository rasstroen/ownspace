<?php

namespace BLL\Queue;

use Application\Database\Factory;
use BLL\Base;

class Worker extends Base
{
    /**
     * @var Factory
     */
    private $db = 'db';

    /**
     * @param int $workerId
     * @param int $task_id
     * @param array $taskData
     * @param int $delay
     */
    public function addTask(int $workerId, int $taskId, array $taskData = [], int $delay = 0)
    {
        $data = ['task_id' => $taskId];
        $data['run_time'] = time() + $delay;
        $data['task'] = serialize($taskData);
        $this->db->gen->insert($this->getTable($workerId), $data, $data);
    }

    /**
     * @param int $workerId
     * @param int $count
     * @return array
     */
    public function getTasks(int $workerId, int $count)
    {
        return $this->db->gen->selectKeyRow(
            'SELECT * FROM ' . $this->getTable($workerId) . ' WHERE `run_time` < ? ORDER BY `run_time` LIMIT ?',
            [
                time(),
                $count
            ],
            'task_id'
        );
    }

    public function deleteTasks(int $workerId, array $taskIds)
    {
        return $this->db->gen->deleteByIds(
            $this->getTable($workerId),
            $taskIds,
            'task_id'
        );
    }

    /**
     * @param int $workerId
     * @return string
     */
    private function getTable(int $workerId)
    {
        return 'tasks_' . $workerId;
    }
}