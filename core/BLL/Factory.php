<?php
namespace BLL;

use Application\InjectableComponentsFactory;

class Factory extends InjectableComponentsFactory
{
    /**
     * @var array
     */
    private $bllClassesConfiguration = array();


    public function __construct(array $bllClassesConfiguration)
    {
        $this->bllClassesConfiguration = $bllClassesConfiguration;
    }
}