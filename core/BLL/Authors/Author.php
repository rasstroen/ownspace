<?php

namespace BLL\Authors;

use Application\Database\Factory;
use BLL\Base;

class Author extends Base
{
    /**
     * @var Factory
     */
    private $db = 'db';

    /**
     * @param string $username
     * @return array|null
     */
    public function getByUsername(string $username)
    {
        return $this->db->web->selectRow('SELECT * FROM `author` WHERE `username` = ?', [$username]);
    }

    /**
     * @param int $authorId
     */
    public function getById(int $authorId)
    {
        return $this->db->web->selectRow('SELECT * FROM `author` WHERE `author_id` = ?', [$authorId]);
    }

    public function getProfileFoafUrlByUsername(string $username)
    {
        return 'http://' . $username . '.livejournal.com/data/foaf';
    }

    /**
     * @param string $username
     * @return mixed
     */
    public function addByUsername(string $username)
    {
        $this->db->gen->insert('author', ['username' => $username], ['username' => $username]);
        $authorId = $this->db->gen->lastInsertId();
        if (!$authorId) {

            $author = $this->getByUsername($username);
            echo 'ADDED' . print_r($author);
            if ($author) {
                return $author['author_id'];
            }
        }
        return $authorId;
    }
}