<?php
namespace Application\Util;

class UtilString
{
    /**
     * @param string $word
     * @param string $allowedChars
     * @return mixed
     */
    public static function camelize(string $word, string $allowedChars = '')
    {
        return str_replace(' ', '',
            ucwords(preg_replace('/[^A-Za-z0-9' . preg_quote($allowedChars, '/') . ']+/', ' ', $word)));
    }

    /**
     * make namespace string from strings, example:
     * queue-server => Queue\Server
     * queueServer => Queue\Server
     *
     * @param $word
     * @return string
     */
    public static function toNamespace(string $word)
    {
        return preg_replace('/[\s\-]+/', '\\', ucwords(preg_replace(['/[A-Z]+/', '/\-/'], [' $0', ' '], $word)));
    }
}
