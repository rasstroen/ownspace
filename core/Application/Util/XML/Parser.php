<?php
namespace Application\Util\XML;

class Parser extends \FastXml\Parser
{
    public function startTag($parser, $name)
    {
        if ($name == $this->endTag) {
            $this->currentData = [];
        }
        return parent::startTag($parser, $name);
    }

    public function tagData($parser, $data)
    {
        if ($this->currentTag) {
            if (!isset($this->currentData[$this->currentTag])) {
                $this->currentData[$this->currentTag] = trim($data);
            }
        }
    }
}