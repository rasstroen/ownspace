<?php
namespace Application;

/**
 * Фабрика, которая хранит в себе компоненты, описанные в конфигурации
 *
 * Class Component
 * @package Application
 */
abstract class InjectableComponentsFactory extends InjectableComponent
{
    /**
     * @var string
     */
    protected $componentsFolder;

    /**
     * @var Injector
     */
    protected $injector = 'injector';

    /**
     * @var Configuration
     */
    protected $configuration = 'configuration';

    public function initialize(array $configuration)
    {
        if (!isset($configuration['componentsFolder'])) {
            throw new \Exception('componentsFolder is not set for factory configuration');
        }
        $this->componentsFolder = $configuration['componentsFolder'];
    }

    /**
     * @param string $componentName
     * @return InjectableComponent
     */
    public function __get(string $componentName)
    {
        if (!isset($this->components[$componentName])) {
            $componentSettings = $this->configuration->getSubComponentSettings($this->componentsFolder, $componentName);
            $component = new $componentSettings['class']($componentSettings);
            $this->injector->inject($component);
            $this->components[$componentName] = $component;
        }
        return $this->components[$componentName];
    }
}