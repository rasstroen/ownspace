<?php
namespace Application;

use Application\Util\UtilArray;

class Configuration extends InjectableComponent
{
    const CONFIG_COMPONENTS_FILENAME = 'components.php';
    const CONFIG_SITEMAP_FILENAME = 'sitemap.php';
    const CONFIG_PAGES_FILENAME = 'pages.php';
    const CONFIG_PATHS_FILENAME = 'paths.php';
    const CONFIG_SETTINGS_FILENAME = 'settings.php';
    /**
     * @var string
     */
    private $configurationPath;

    /**
     * @var array
     */
    private $configuration = [];

    public function getStaticPath($type)
    {
        return $this->getWebPath('static', '/static/') . $type . '/';
    }

    /**
     * @return sring
     */
    public function getTemplatesPath()
    {
        return $this->getPath('templates');
    }

    public function getTemplatesCachePath()
    {
        return $this->getPath('templates_cache', 'templates/cache');
    }

    public function isDebugMode()
    {
        return $this->getSetting('debug', false);
    }

    public function getDbconnectionProperty(string $connectionName, string $propertyName, string $defaultValue = null)
    {
        if (empty($this->configuration['settings'])) {
            $this->configuration['settings'] = require_once $this->configurationPath . self::CONFIG_SETTINGS_FILENAME;
        }
        return $this->configuration['settings']['db'][$connectionName][$propertyName] ?? $defaultValue;
    }

    /**
     * @param $settingName
     * @param null $defaultValue
     * @return null
     */
    public function getSetting(string $settingName, $defaultValue = null)
    {
        if (empty($this->configuration['settings'])) {
            $this->configuration['settings'] = require_once $this->configurationPath . self::CONFIG_SETTINGS_FILENAME;
        }
        return $this->configuration['settings'][$settingName] ?? $defaultValue;
    }

    /**
     * @param $pathName
     * @param $defaultValue
     * @return string
     */
    private function getPath(string $pathName, string $defaultValue = null)
    {
        if (empty($this->configuration['paths'])) {
            $this->configuration['paths'] = require_once $this->configurationPath . self::CONFIG_PATHS_FILENAME;
        }
        return (string)$this->configurationPath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . ($this->configuration['paths'][$pathName] ?? $defaultValue);
    }

    private function getWebPath(string $pathName, string $defaultValue = null)
    {
        if (empty($this->configuration['paths'])) {
            $this->configuration['paths'] = require_once $this->configurationPath . self::CONFIG_PATHS_FILENAME;
        }
        return '/' . ($this->configuration['paths'][$pathName] ?? $defaultValue) . '/';
    }

    public function getPageSettings(string $pageIndex)
    {
        if (empty($this->configuration['pages'])) {
            $this->configuration['pages'] = require_once $this->configurationPath . self::CONFIG_PAGES_FILENAME;
        }
        $configuration = $this->configuration['pages']['pages'][$pageIndex];
        $configuration = $this->applyPageDefaults($configuration, $this->configuration['pages']['defaults']);
        return $configuration ?? null;
    }

    private function applyPageDefaults(array $pageSettings, array $defaults)
    {
        $layout = $pageSettings['layout'];
        if (isset($defaults[$layout])) {
            $pageSettings = UtilArray::mergeArrays([$defaults[$layout], $pageSettings]);
        }
        return $pageSettings;
    }

    /**
     * @param $configurationPath
     */
    public function __construct(string $configurationPath)
    {
        $this->configurationPath = $configurationPath;
    }


    public function getSubComponentSettings(string $componentsFolder, string $componentName)
    {
        if (empty($this->configuration['components'])) {
            $this->configuration['components'] = require_once $this->configurationPath . self::CONFIG_COMPONENTS_FILENAME;
        }
        return $this->configuration['components'][$componentsFolder][$componentName] ?? null;
    }

    /**
     * @param $componentName
     * @return null
     */
    public function getComponentSettings(string $componentName)
    {
        if (empty($this->configuration['components'])) {
            $this->configuration['components'] = require_once $this->configurationPath . self::CONFIG_COMPONENTS_FILENAME;
        }
        return $this->configuration['components'][$componentName] ?? null;
    }

    /**
     * @return null|array
     */
    public function getSiteMap()
    {
        if (empty($this->configuration['sitemap'])) {
            $this->configuration['sitemap'] = require_once $this->configurationPath . self::CONFIG_SITEMAP_FILENAME;
        }
        return $this->configuration['sitemap'] ?? null;
    }
}