<?php
namespace Application\Torg\Component\ContentApi;

use Application\InjectableComponent;

class Client extends InjectableComponent
{
    private $token;
    private $baseUrl;
    private $queryString;

    public function setToken(string $token)
    {
        $this->token = $token;
    }

    public function getQueryString()
    {
        return $this->queryString;
    }

    public function setBaseUrl($url)
    {
        $this->baseUrl = $url;
    }


    public function request(string $path, array $query = array())
    {
        $path = $this->baseUrl . $path;
        if ($query) {
            $path .= '?' . http_build_query($query);
        }
        $ch = curl_init($path);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization:' . $this->token));
        $this->queryString = $path;
        ob_start();
        curl_exec($ch);
        $response = ob_get_clean();
        if ($errorMessage = curl_error($ch)) {
            throw new \Exception($errorMessage);
        }
        $info = curl_getinfo($ch);
        if ($info['http_code'] != 200) {
            throw new \Exception($info['http_code'] . ' code in response for ' . $this->queryString);
        }
        return $response;
    }
}