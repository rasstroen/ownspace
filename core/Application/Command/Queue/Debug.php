<?php
namespace Application\Command\Queue;

use Application\Command\Queue\Workers\Livejournal\AuthorFetchRss;
use Application\Command\Queue\Workers\Livejournal\EntriesAPI;
use Application\Command\Queue\Workers\Worker;
use Application\Injector;

class Debug
{
    /**
     * @var Injector
     */
    private $injector = 'injector';

    /**
     * @var \BLL\Queue\Worker
     */
    private $bllWorker = 'bll.worker';

    public function actionEntriesApi()
    {
        $worker = new EntriesAPI();
        $this->injector->inject($worker);
        $worker->initialize();
        $tasks = [];

        $worker->run($tasks);
    }

    public function actionAuthorFetchRss()
    {
        $worker = new AuthorFetchRss();
        $this->injector->inject($worker);
        $worker->initialize();
        $tasks = $this->bllWorker->getTasks(Server::WORKER_ID_AUTHOR_FETCH_RSS, 3);
        $tasks  = [];
        $tasks[3] =[703];
        $worker->run($tasks);
        if (count($tasks)) {
            $this->bllWorker->deleteTasks(Server::WORKER_ID_AUTHOR_FETCH_RSS, array_keys($tasks));
        }
    }
}
