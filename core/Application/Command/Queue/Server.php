<?php
namespace Application\Command\Queue;

use Application\Command\Queue\Workers\Livejournal\AuthorFetchRss;
use Application\Command\Queue\Workers\Livejournal\EntriesAPI;
use Application\Command\Queue\Workers\Test;
use Application\Command\Queue\Workers\Worker;
use Application\Configuration;
use Application\Injector;

class Server
{
    const STATUS_FILE_WRITE_PERIOD = 10;

    private $file;
    /**
     * @var Configuration
     */
    private $configuration = 'configuration';

    /**
     * @var Injector
     */
    private $injector = 'injector';

    /**
     * @var \BLL\Queue\Worker
     */
    private $bllWorker = 'bll.worker';

    /**
     * @var array
     */
    private $workersStatus = [];

    /**
     * @var array
     */
    private $currentJobs = [];

    const WORKER_ID_ENTRIES_API = 1;
    const WORKER_ID_AUTHOR_FETCH_RSS = 2;
    const WORKER_ID_TEST = 100;
    private $workers = [
        self::WORKER_ID_ENTRIES_API      => [
            'name'        => 'entries-api',
            'class'       => EntriesAPI::class,
            'action'      => 'parse',
            'max_workers' => 1,
            'interval'    => 1800,
        ],
        self::WORKER_ID_AUTHOR_FETCH_RSS => [
            'name'           => 'author-fetch-rss',
            'class'          => AuthorFetchRss::class,
            'action'         => 'parse',
            'max_workers'    => 1,
            'interval'       => 0,
            'tasksPerWorker' => 10,
        ],
        self::WORKER_ID_TEST             => [
            'name'        => 'test',
            'class'       => Test::class,
            'action'      => 'test',
            'max_workers' => 2,
            'interval'    => 10
        ],
    ];

    private function log($message)
    {
        echo date('Y-m-d H:i:s') . ' ' . $message . "\n";
    }

    private function checkIfRunning()
    {
        $time = time();
        list($mtime, $pid) = file($this->configuration->getSetting('queue')['status_file']);
        if (($time - $mtime) < (self::STATUS_FILE_WRITE_PERIOD * 2)) {
            if (getmypid() != $pid) {
                $this->log('Terminating - status file modify time changed ' . ($time - $mtime));
                exit(0);
            }
        } else {
            $this->log('Status file changed seconds:' . ($time - $mtime));
        }
    }

    private function updateStatusFile($truncate = false)
    {
        $time = time();
        $fileName = $this->configuration->getSetting('queue')['status_file'];
        $this->file = fopen($fileName, 'w');
        fwrite($this->file,
            ($truncate ? 0 : $time) . "\n" . getmypid() . "\n" . json_encode([
                'workersStatus' => $this->workersStatus,
                'workers'       => $this->workers,
                'jobs'          => $this->currentJobs
            ]));
    }

    public function actionRun()
    {
        $this->log('running server');
        $this->renewConfiguration();
        $timeStatus = time() - self::STATUS_FILE_WRITE_PERIOD;
        $this->log('timeStatus ' . $timeStatus);
        while (true) {
            $time = time();
            $timeLeft = $time - $timeStatus;
            if ($timeLeft >= self::STATUS_FILE_WRITE_PERIOD) {
                $this->log('updating status file ' . $timeLeft);
                $timeStatus = $time;
                $this->checkIfRunning();
                $this->updateStatusFile();
            }
            $this->runWorkers();
        }
    }

    private function renewConfiguration()
    {
        // @todo
    }

    private function runWorkers()
    {
        $runned = true;

        while ($runned) {
            $time = time();
            $runned = false;
            sleep(1);
            foreach ($this->workers as $workerId => $workerConfiguration) {


                $count = count($this->workersStatus[$workerId]['workers'] ?? []);

                if (!empty($this->workersStatus[$workerId]['workers']) && (count($this->workersStatus[$workerId]['workers']) >= $workerConfiguration['max_workers'])) {
                    continue;
                }


                if (!empty($this->workersStatus[$workerId]['last_run'])) {
                    if ($time - $this->workersStatus[$workerId]['last_run'] < $workerConfiguration['interval']) {
                        continue;
                    }
                }

                $this->log($workerConfiguration['class'] . " workers " . $count . " from " . $workerConfiguration['max_workers']);

                $runned = true;
                $pid = pcntl_fork();
                if ($pid == -1) {
                    throw new Exception('could not fork');
                } else {
                    if ($pid) {
                        $this->workersStatus[$workerId]['workers'][$pid] = 1;
                        $this->workersStatus[$workerId]['last_run'] = $time;
                        $this->workersStatus[$workerId]['last_run_date'] = date('Y-m-d H:i:s', $time);
                        $this->currentJobs[$pid] = $workerId;
                        continue;
                    } else {
                        $this->runWorker($workerId, $workerConfiguration);
                    }
                }
            }
        }
        foreach ($this->currentJobs as $pid => $workerId) {
            if (($pid = pcntl_waitpid($pid, $status, WNOHANG)) > 0) {
                unset($this->workersStatus[$this->currentJobs[$pid]]['workers'][$pid]);
                unset($this->currentJobs[$pid]);
            }
        }
    }

    /**
     * @param $workerId
     * @return array
     */
    private function getWorkerTasks($workerId)
    {
        $tasks = [1];
        if (!empty($this->workers[$workerId]['tasksPerWorker'])) {
            $tasks = $this->bllWorker->getTasks($workerId, $this->workers[$workerId]['tasksPerWorker']);
            $this->bllWorker->deleteTasks($workerId, array_keys($tasks));
        }
        return $tasks;
    }

    public function runWorker(int $workerId, array $workerConfiguration)
    {
        $worker = new $workerConfiguration['class'];
        $this->injector->inject($worker);
        $tasks = $this->getWorkerTasks($workerId);
        /**
         * @var Worker $worker
         */
        if (count($tasks)) {
            $worker->initialize();
            $worker->run($tasks);
        }
        exit();
    }
}
