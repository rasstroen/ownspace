<?php
namespace Application\Command\Queue\Workers\Livejournal;

use Application\Command\Queue\Workers\Worker;
use Application\Http\Client;
use BLL\Queue\Author;
use FastXml\CallbackHandler\GenericHandler;

class EntriesAPI extends Worker
{
    /**
     * @var Client
     */
    private $httpClient = 'httpClient';

    /**
     * @var Author
     */
    private $bllQueueAuthor = 'bll.queueAuthor';
    /**
     * @var \BLL\Authors\Author
     */
    private $bllAuthor = 'bll.author';


    /**
     * @var array
     */
    private $options = [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_SSL_VERIFYHOST => false
    ];

    const URL = 'http://blogs.yandex.ru/entriesapi';
    const LOCAL_CACHE_TIME = 3600;
    const PAGE_PARAMETER_NAME = 'p';

    public function run(array $tasks)
    {
        $this->log(self::URL);
        $page = 1;
        do {
            $xmlContent = $this->httpClient->getLocallyCached(self::URL . '?' . self::PAGE_PARAMETER_NAME . '=' . $page,
                $this->options,
                self::LOCAL_CACHE_TIME);
            $entries = $this->parse($xmlContent);
            $entriesCount = count($entries);
            $this->log($entriesCount . ' entries on page ' . $page);
            foreach ($entries as $entry) {
                if ($this->isLivejournalEntry($entry)) {
                    $this->addTaskToProcessAuthor($entry);
                }
            }
            $page++;
        } while ($entriesCount);
    }

    private function addTaskToProcessAuthor(array $entry)
    {
        if (!empty($entry['yablogs:ppb_username'])) {
            $author = $this->bllAuthor->getByUsername($entry['yablogs:ppb_username']);
            if (!$author) {
                $authorId = $this->bllAuthor->addByUsername($entry['yablogs:ppb_username']);
            } else {
                $authorId = $author['author_id'];
            }
            return $this->bllQueueAuthor->addTaskToProcessAuthorFeed($authorId);
        }
        return false;
    }

    private function isLivejournalEntry($entry)
    {
        return strpos($entry['author'], 'livejournal.com') > 0;
    }

    private function parse(string $xmlContent = null)
    {
        $entries = [];
        $localTempFileName = $this->configuration->getSetting('cachePath', '/tmp/cache/') . 'entriesapi.tmp';
        file_put_contents($localTempFileName, $xmlContent);
        $handler = new GenericHandler();
        $parser = new \Application\Util\XML\Parser($handler);
        $parser->setIgnoreTags(['channel', 'generator']);
        $handler->setOnItemParsedCallback(function ($item) use (&$entries) {
            $entries[] = $item;
        });
        $parser->setEndTag('item');
        $parser->parse($localTempFileName);
        return $entries;
    }
}