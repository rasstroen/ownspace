<?php
namespace Application\Command\Queue\Workers\Livejournal;

use Application\Command\Queue\Workers\Worker;
use Application\Http\Client;
use BLL\Authors\Author;
use FastXml\CallbackHandler\GenericHandler;

class AuthorFetchRss extends Worker
{
    const CACHE_TIME = 1800;
    /**
     * @var Configuration
     */
    protected $configuration = 'configuration';

    /**
     * @var Author
     */
    protected $bllAuthor = 'bll.author';


    /**
     * @var Client
     */
    protected $httpClient = 'httpClient';

    /**
     * @var array
     */
    protected $options = [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_SSL_VERIFYHOST => false
    ];

    public function run(array $tasks)
    {
        foreach ($tasks as $authorId => $task) {
            $this->log('working on author id: ' . $authorId);
            $author = $this->bllAuthor->getById($authorId);
            $url = $this->bllAuthor->getProfileFoafUrlByUsername($author['username']);
            $xmlContent = $this->httpClient->getLocallyCached($url, $this->options, self::CACHE_TIME,
                $this->configuration->getSetting('cachePath', '/tmp/cache/') . '/author_rss/');
            if (strpos($xmlContent, 'foaf:Group')) {
                $author = $this->parseGroup($xmlContent);
            } else {
                $author = $this->parsePerson($xmlContent);
            }
            die(print_r($author));
        }
    }

    private function parseGroup(string $xmlContent = null)
    {
        $entries = [];
        $localTempFileName = $this->configuration->getSetting('cachePath', '/tmp/cache/') . 'author.tmp';
        file_put_contents($localTempFileName, $xmlContent);
        $handler = new GenericHandler();
        $parser = new \Application\Util\XML\Parser($handler);
        $parser->setIgnoreTags(['foaf:member_name', 'foaf:tagLine', 'foaf:image', 'foaf:nick', 'foaf:knows']);
        $handler->setOnItemParsedCallback(function ($item) use (&$entries) {
            die(print_r($item));
        });
        $parser->setEndTag('foaf:Group');
        $parser->parse($localTempFileName);
        return $entries;
    }

    private function parsePerson(string $xmlContent = null)
    {
        $xml = simplexml_load_string(str_replace(['lj:', 'foaf:', 'rdf:', 'dc:', 'ya:'], '', $xmlContent));
        print_r($xml);
        $author['username'] = (string)($xml->Person->nick);
        $author['name'] = (string)($xml->Person->name);
        $author['picture'] = (string)($xml->Person->img->attributes()[0]);
        $author['birthday'] = (string)($xml->Person->dateOfBirth);
        $author['homepage'] = $xml->Person->homepage ? (string)($xml->Person->homepage->attributes()->resource) : '';
        $author['homepage_title'] = $xml->Person->homepage ? (string)($xml->Person->homepage->attributes()->title) : '';
        $author['journaltitle'] = (string)(string)$xml->Person->journaltitle;
        $author['journalsubtitle'] = (string)(string)$xml->Person->journalsubtitle;
        $author['date_created'] = strtotime((string)$xml->Person->weblog->attributes()->dateCreated);
        $author['date_last_updated'] =  strtotime((string)$xml->Person->weblog->attributes()->dateLastUpdated);
        $author['country'] = (string)$xml->Person->country->attributes()->title;
        $author['city'] = urldecode((string)$xml->Person->city->attributes()->title);
        $author['bio'] = (string)$xml->Person->bio;


        $author['posts'] = (string)$xml->Person->blogActivity->Posts->posted;
        $author['comments_posted'] = (string)$xml->Person->blogActivity->Comments->posted;
        $author['comments_received'] = (string)$xml->Person->blogActivity->Comments->received;



        foreach ($xml->Person->interest as $i) {
            $author['interests'][(string)$i->attributes()->title] = (string)$i->attributes()->title;
        }

        return $author;
    }
}