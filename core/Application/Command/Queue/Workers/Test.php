<?php
namespace Application\Command\Queue\Workers;

class Test extends Worker
{
    public function run(array $tasks)
    {
        $pid = $this->getPid();
        sleep(5);
        echo "test worker $pid done\n";
    }
}