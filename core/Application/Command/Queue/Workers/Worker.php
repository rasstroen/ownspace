<?php
namespace Application\Command\Queue\Workers;

use Application\Configuration;

abstract class Worker
{
    /**
     * @var int
     */
    protected $pid;

    /**
     * @var Configuration
     */
    protected $configuration = 'configuration';

    abstract public function run(array $tasks);

    public function __construct()
    {
        $this->pid = getmypid();
        $this->log("worker " . static::class . " {$this->pid} start");
    }

    public function initialize()
    {
        $errorLogDirName = $this->configuration->getSetting('cachePath', '/tmp/log/');

        $errorLogFileName = $errorLogDirName . 'workers.error.log';
        if (!file_exists($errorLogFileName)) {
            mkdir($errorLogDirName);
        }
        register_shutdown_function(function () use ($errorLogFileName) {
            $error = error_get_last();
            if ($error) {
                $errorMessage = $error['message'] . " " . $error['file'] . ':' . $error['line'] . " " . print_r($error,
                        true);
                $handle = fopen($errorLogFileName, "a");
                fwrite($handle, $errorMessage . "\n");
            }
        });
    }

    protected function log($message)
    {
        echo date('Y-m-d H:i:s') . ' ' . $message . "\n";
    }

    protected function getPid()
    {
        return $this->pid;
    }
}