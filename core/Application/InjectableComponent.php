<?php
namespace Application;

/**
 *
 * Class Component
 * @package Application
 */
class InjectableComponent
{
    /**
     * @var array
     */
    protected $components = [];

    public function initialize(array $configuration)
    {

    }
}