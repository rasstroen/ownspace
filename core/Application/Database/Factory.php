<?php
namespace Application\Database;

use Application\Configuration;
use Application\InjectableComponent;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;

/**
 * Class Configuration
 * @package Application\Database
 *
 * @property  \Application\Database\Connection $web
 * @property  \Application\Database\Connection $gen
 *
 */
class Factory extends InjectableComponent
{
    /**
     * @var Configuration
     */
    private $configuration = 'configuration';

    private $connections = [];

    public function __get(string $connectionName)
    {
        if (!isset($this->connections[$connectionName])) {
            $this->connections[$connectionName] = $this->connect($connectionName);
        }
        return $this->connections[$connectionName];
    }

    private function connect($connectionName)
    {
        $connectionParams = array(
            'dbname'   => $this->configuration->getDbconnectionProperty($connectionName, 'dbname'),
            'user'     => $this->configuration->getDbconnectionProperty($connectionName, 'user'),
            'password' => $this->configuration->getDbconnectionProperty($connectionName, 'password'),
            'host'     => $this->configuration->getDbconnectionProperty($connectionName, 'host'),
            'driver'   => $this->configuration->getDbconnectionProperty($connectionName, 'driver'),
        );
        return new \Application\Database\Connection(DriverManager::getConnection($connectionParams));
    }
}