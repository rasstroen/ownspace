<?php
namespace Application;

use Application\Util\UtilString;

/**
 * Class Web
 * @package Application
 */
class Console extends Base
{
    public function run()
    {
        global $argv;
        $command = $argv[1] ?? null;
        $action = 'action' . ucfirst($argv[2] ?? 'index');

        if (!$command) {
            throw new \Exception('command is not defined');
        }

        $commandName = UtilString::toNamespace($command);
        $commandClass = "Application\\Command\\{$commandName}";
        $command = new $commandClass;
        $this->injector->inject($command);
        $command->$action();
    }
}
