<?php
namespace Application;

use Application\Http\Request;
use Application\Routing\Exception\Corrected;
use Application\Routing\Router;
use Application\View\Helper;

/**
 * Class Web
 * @package Application
 */
class Web extends Base
{
    /**
     * @var Router
     */
    protected $router = 'router';

    /**
     * @var Controller\Web
     */
    protected $controller = 'controller';
    /**
     * @var Request
     */
    private $request = 'request';

    /**
     * @var Helper
     */
    private $view = 'view';

    public function run()
    {
        $this->processRequest();
        $this->doResponse();
    }

    private function doResponse()
    {
        $this->sendHeaders();
        $this->view->render();
    }

    private function sendHeaders()
    {
        foreach ($this->request->getHeaders() as $header) {
            header($header);
        }
    }

    private function processRequest()
    {
        try {
            $pageSettings = $this->router->getPageSettings();
        } catch (Corrected $exception) {
            $this->request->redirect($exception->getCorrectedUri());
            return;
        }
        $this->controller->executePage($pageSettings['pageIndex'], $pageSettings);
    }
}