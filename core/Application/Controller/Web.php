<?php
namespace Application\Controller;

use Application\InjectableComponent;
use Application\Configuration;
use Application\Injector;
use Application\Routing\Exception\NotFound;
use Application\Util\UtilArray;

/**
 *
 * Class Web
 * @package Application\Controller
 */
class Web extends InjectableComponent
{
    /**
     * @var Configuration
     */
    private $configuration = 'configuration';
    /**
     * @var array
     */
    private $variables = [];

    /**
     * @var array
     */
    private $responseData = [];

    /**
     * @var Injector
     */
    private $injector = 'injector';

    /**
     * @var array
     */
    private $pageSetting = [
        'css' => [],
        'js'  => []
    ];

    public function getResponseData()
    {
        return $this->responseData;
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        return $this->pageSettings['layout'];
    }

    /**
     * @return array
     */
    public function getPageSettings()
    {
        return $this->pageSettings;
    }

    public function runModule(string $blockName, string $moduleName, array $moduleSettings)
    {
        if (empty($moduleSettings['class'])) {
            throw new \Exception('no class property');
        }

        $action = 'action' . ucfirst($moduleSettings['action'] ?? 'index');
        $module = new $moduleSettings['class']($this->variables);

        $this->injector->inject($module);

        $this->responseData[$blockName][$moduleName] = $module->$action();
        $this->responseData[$blockName][$moduleName]['template'] = 'modules/' . $moduleSettings['template'];
    }

    public function runModules(array $blocks)
    {
        foreach ($blocks as $blockName => $modules) {
            foreach ($modules as $moduleName => $moduleSettings) {
                $this->runModule($blockName, $moduleName, $moduleSettings);
            }
        }
    }

    public function executePage(string $pageIndex, array $pageConfiguration)
    {
        if (!empty($pageConfiguration['variables'])) {
            $this->variables = $pageConfiguration['variables'];
        }
        $pageSettings = $this->configuration->getPageSettings($pageIndex);
        if (null === $pageSettings) {
            throw new NotFound('Cant get settings for page "' . $pageIndex . '"');
        }

        if (empty($pageSettings['layout'])) {
            throw new \Exception('missing layout for page "' . $pageIndex . '"');
        }

        if (!empty($pageSettings['blocks'])) {
            $this->runModules($pageSettings['blocks']);
        }

        $this->pageSettings = UtilArray::mergeArrays([$pageSettings, $this->pageSetting]);
    }
}