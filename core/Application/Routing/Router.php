<?php
namespace Application\Routing;

use Application\InjectableComponent;
use Application\Configuration;
use Application\Http\Request;
use Application\Routing\Exception\Corrected;
use Application\Routing\Exception\NotFound;

/**
 * Class Router
 * @package Application\Routing
 */
class Router extends InjectableComponent
{

    /**
     * @var Configuration
     */
    private $configuration = 'configuration';

    /**
     * @var Request
     */
    private $request = 'request';

    /**
     * �� �������� URL ������� ������ ��� ��������� �������� � ������ � ��������� �� ������������
     */
    public function getPageSettings()
    {
        $siteMap = $this->configuration->getSiteMap();
        $requestUri = $this->request->getRequestUri();
        $queryString = '';
        $uriParts = [];
        $uri = $requestUri;
        if (strpos($requestUri, '?')) {
            $uri = explode('?', $requestUri);
            if (!empty($uri[1])) {
                $queryString = '?' . $uri[1];
            }
            $uri = $uri[0];
        }

        $requestUriParts = explode('/', $uri);

        $this->getPageIdByRequestArray($requestUriParts, $path, $variables, $uriParts, $siteMap);
        if ((count($path) == 1) && (count($requestUriParts) > 0)) {
            throw new NotFound();
        }

        foreach ($path as $part) {
            $siteMap = $siteMap[$part];
        }

        if (is_array($siteMap)) {
            throw new NotFound;
        }

        $idealRequestUri = $this->getIdealRequestUri($uriParts, $queryString);

        if ($idealRequestUri !== $requestUri) {
            throw new Corrected($idealRequestUri);
        }

        $pageSettings['pageIndex'] = $siteMap;
        $pageSettings['variables'] = $variables;

        return $pageSettings;
    }

    /**
     * @param array $uriParts
     * @param $queryString
     * @return string
     */
    private function getIdealRequestUri(array $uriParts, string $queryString)
    {
        $idealUri = '/' . implode('/', $uriParts) . $queryString;
        return $idealUri ?: '/';
    }

    private function processCurrentSitemapPart($sitemapPart, $currentRequestUriPart, &$variables)
    {
        if (is_array($sitemapPart)) {
            if (isset($sitemapPart['_var'])) {
                $variables[$sitemapPart['_var']] = $currentRequestUriPart;
            }
        }
    }

    private function getPageIdByRequestArray($requestUriParts, &$path, &$variables, &$uriParts, $siteMap)
    {
        if (!is_array($siteMap)) {
            return true;
        }

        $currentRequestUriPart = array_shift($requestUriParts);

        foreach ($siteMap as $siteMapId => $siteMapPart) {
            if (
                (
                    $siteMapId == $currentRequestUriPart)
                ||
                ($siteMapId == '%d') && (intval($currentRequestUriPart) . '' === $currentRequestUriPart)
                ||
                ($siteMapId == '%s')
            ) {

                $path[] = $siteMapId;
                if ('' !== trim($currentRequestUriPart)) {
                    $uriParts[] = $currentRequestUriPart;
                }
                $this->processCurrentSitemapPart($siteMapPart, $currentRequestUriPart, $variables);
                return $this->getPageIdByRequestArray($requestUriParts, $path, $variables, $uriParts,
                    $siteMap[$siteMapId]);
            }
        }
    }
}