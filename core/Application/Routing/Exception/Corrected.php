<?php
namespace Application\Routing\Exception;

class Corrected extends \Exception
{
    private $correctedUri;

    /**
     * @param $correctedUri
     */
    public function __construct(string $correctedUri)
    {
        $this->correctedUri = $correctedUri;
    }

    public function getCorrectedUri()
    {
        return $this->correctedUri;
    }
}