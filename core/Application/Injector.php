<?php

namespace Application;

class Injector
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var InjectableComponent[]
     */
    private $components;

    public function setConfiguration(Configuration $configuration)
    {
        /**
         * Класс конфигурации уже создан и еще раз создавать его не будем
         */
        $this->components['configuration'] = $this->configuration = $configuration;
        /**
         * сам инжектор нужен в некоторых классах
         */
        $this->components['injector'] = $this;
    }

    public function inject($object)
    {
        $class = new \ReflectionClass($object);
        /**
         * перебираем только приватные и защищенные свойства
         */
        foreach ($class->getProperties(\ReflectionProperty::IS_PRIVATE | \ReflectionProperty::IS_PROTECTED) as $property) {
            $property->setAccessible(true);
            $propertyValue = $property->getValue($object);
            /**
             * Если значение - строка
             */
            if (!empty($propertyValue) && is_string($propertyValue)) {
                /**
                 * Разбиваем по точке значение
                 */
                $parent = $this;
                foreach (explode('.', $property->getValue($object)) as $componentLevel) {
                    $parent = $parent->$componentLevel;
                }
                if (false !== $parent) {
                    /**
                     * Устанавливаем значением свойства компонент, если нашли такой компонент по имени
                     */
                    $property->setValue($object, $parent);
                }
            }
            $property->setAccessible(false);
        }
    }

    /**
     * @param $componentLevel
     * @return Component|bool
     */
    public function __get($componentLevel)
    {
        if (!isset($this->components[$componentLevel])) {
            $componentConfiguration = $this->configuration->getComponentSettings($componentLevel);
            if (empty($componentConfiguration) || !isset($componentConfiguration['class'])) {
                /**
                 * Не нашли такого компонента
                 */
                return false;
            }
            $this->components[$componentLevel] = new $componentConfiguration['class']($componentConfiguration);

            /**
             * Инжектим все свойства компонента
             */
            if ($this->components[$componentLevel] instanceof InjectableComponent) {
                $this->inject($this->components[$componentLevel]);
            }
            /**
             * вызываем initialize() компонента уже после всех инжектов в класс
             */
            $this->components[$componentLevel]->initialize($componentConfiguration);
        }
        return $this->components[$componentLevel];
    }
}