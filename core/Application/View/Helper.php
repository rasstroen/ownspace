<?php
namespace Application\View;

use Application\InjectableComponent;
use Application\Configuration;
use Application\Controller\Web;

/**
 * Class Helper
 * @package Application\View
 *
 */
class Helper extends InjectableComponent
{
    /**
     * @var Web
     */
    private $controller = 'controller';

    /**
     * @var Configuration
     */
    private $configuration = 'configuration';

    private function preparePageSettings(array $pageSettings)
    {
        $pageSettings['css'] = $pageSettings['css'] ?? [];
        $pageSettings['js'] = $pageSettings['js'] ?? [];
        foreach (['css', 'js'] as $type) {
            foreach ($pageSettings[$type] as &$item) {
                $item = $this->configuration->getStaticPath($type) . $item;
            }
            unset($item);

        }
        return $pageSettings;
    }

    public function render()
    {
        $layout = $this->controller->getLayout();
        $loader = new \Twig_Loader_Filesystem($this->configuration->getTemplatesPath());

        $twig = new \Twig_Environment($loader, array(
            '_cache' => $this->configuration->getTemplatesCachePath()
        ));
        $template = $twig->loadTemplate('layouts/bootstrap.twig');

        $pageSettings = $this->preparePageSettings($this->controller->getPageSettings());
        echo $template->render(
            [
                'layout' => 'layouts' . DIRECTORY_SEPARATOR . $layout . '.twig',
                'data'   => $this->controller->getResponseData(),
                'debug'  => $this->configuration->isDebugMode(),
                'page'   => $pageSettings
            ]
        );
    }
}