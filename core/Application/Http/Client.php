<?php
namespace Application\Http;

use Application\Configuration;
use Application\InjectableComponent;

class Client extends InjectableComponent
{
    /**
     * @var Configuration
     */
    private $configuration = 'configuration';

    public function getLocallyCached(string $url, array $options, int $cacheTime = 60, string $cacheFolder = null)
    {
        $localTempFileName = ($cacheFolder ?: $this->configuration->getSetting('cachePath', '/tmp/cache/')) . sha1($url);

        if (is_readable($localTempFileName) && (time() - fileatime($localTempFileName) < $cacheTime)) {
            return file_get_contents($localTempFileName);
        }
        @unlink($localTempFileName);
        file_put_contents($localTempFileName, $content = $this->get($url, $options));
        return $content;
    }

    public function get(string $url, array $options)
    {
        $curl = curl_init($url);
        foreach ($options as $option => $value) {
            curl_setopt($curl, $option, $value);
        }
        return curl_exec($curl);
    }
}