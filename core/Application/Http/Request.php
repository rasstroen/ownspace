<?php
namespace Application\Http;

use Application\InjectableComponent;

class Request extends InjectableComponent
{
    /**
     * @var array
     */
    private $queryParams;

    /**
     * @var array
     */
    private $headers = [];

    /**
     * @var string
     */
    private $requestUri;

    /**
     * @var array
     */
    private $post;

    /**
     * @param $uri
     */
    public function redirect(string $uri)
    {
        $this->addHeader('Location', $uri);
    }

    /**
     * @param $name
     * @param $value
     */
    public function addHeader(string $name, string $value)
    {
        $this->headers[] = $name . ': ' . $value;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return string
     */
    public function getRequestUri()
    {
        return $this->requestUri;
    }

    public function getPostParam(string $paramName, $default = null)
    {
        return isset($this->post[$paramName]) ? $this->post[$paramName] : $default;
    }

    public function initialize(array $configuration)
    {
        $this->requestUri = $_SERVER['REQUEST_URI'];

        if (!empty($_GET)) {
            $this->queryParams = $_GET;
        }
        unset($_GET);
        if (!empty($_POST)) {
            $this->post = $_POST;
        }

        unset($_POST);
        unset($_SERVER);
        unset($_FILES);
    }
}