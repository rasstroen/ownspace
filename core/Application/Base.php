<?php
namespace Application;

use Application\Routing\Router;

/**
 * Class Web
 * @package Application
 *
 * @property Configuration $configuration
 */
abstract class Base extends InjectableComponent
{
    /**
     * @var Configuration
     */
    protected $configuration;

    abstract public function run();

    public function __construct(string $configurationFolderPath)
    {
        $this->configuration = new Configuration($configurationFolderPath);
        $this->injector = new Injector();
        $this->injector->setConfiguration($this->configuration);
        $this->injector->inject($this);
    }
}