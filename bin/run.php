<?php
ini_set('display_errors', false);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL);

require_once '../vendor/autoload.php';

$application = new  \Application\Console(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR);

$application->run();
